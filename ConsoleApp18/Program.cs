﻿using System;

namespace ConsoleApp18
{
    class Program
    {
        static void Main(string[] args)
        {
            int[,] arr = new int[8, 8];

            Random r = new Random(); 

            for (int i = 0; i < arr.GetLength(0); i++)
            {
                for (int j = 0; j < arr.GetLength(1); j++)
                {
                    arr[i, j] = r.Next(10, 99);
                    Console.Write($"[{i},{j}]");   
                }
                Console.WriteLine("\n");  
            }

            for (int i = 0; i < arr.GetLength(0); i++)
            {
                for (int j = 0; j < arr.GetLength(1); j++)
                {
                    if(i == j)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                    }
                }
            }

            Console.ForegroundColor = ConsoleColor.White; 
        }
    }
}
